(function($){
    $(document).ready(function () {
        $(".single_add_to_cart_button").attr("disabled", true);
        $(".storefront-sticky-add-to-cart__content-button").attr("disabled", true);
        $("_movprod_bf").prop("disabled", true);
        
        function enable_cb() {
            var checkboxesCHecked = $('.cb-img-block--input:checked').length;
            
            var sList = "";
            if (checkboxesCHecked > 0) {
              $(".single_add_to_cart_button").removeAttr("disabled");
              $(".storefront-sticky-add-to-cart__content-button").removeAttr("disabled");
              $(".btn-movies-woo").hide();
            } else {
              $(".single_add_to_cart_button").attr("disabled", true);
              $(".storefront-sticky-add-to-cart__content-button").attr("disabled", true);
              $(".btn-movies-woo").show();
            }           
            $('.cb-img-block--input').each(function () {
                val = $(this).val();
                if ($(this).is(":checked"))
                {
                    sList += (sList=="" ? val : ", " + val);
                }  
            });

            localStorage.setItem('movies', JSON.stringify(sList));

        }

        $(".cb-img-block--input").click(function() {
            enable_cb(); 
        });

        $(".single_add_to_cart_button").click(
            function() {
               localStorage.setItem('movies', JSON.stringify(movies));
            }
        );

        $movie_field = localStorage.getItem('movies');
        $('#_movprod_bf').val($movie_field);
    });
})(jQuery); 