<?php

/** Block direct access */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Plugin Name: MovProd
 * Description: Extends Woocommerce functionality
 * Version: 1.0.0
 * Author: Alexander Contreras
 */

defined( 'MOVPROD_TEXT_DOMAIN' ) || define( 'MOVPROD_TEXT_DOMAIN', 'movprod' );
defined( 'MOVPROD_BASEPATH' ) || define( 'MOVPROD_BASEPATH', plugin_dir_path( __FILE__ ) );
defined( 'MOVPROD_BASEURI' ) || define( 'MOVPROD_BASEURI', plugin_dir_url( __FILE__ ) );

/** Init */
require_once MOVPROD_BASEPATH .'/vendor/autoload.php';
require_once MOVPROD_BASEPATH . '/includes/init.php';


/** Enqueue plugin styles */
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style( 'movprod_style', MOVPROD_BASEURI . 'assets/css/movprod-main.css', array(), '1.0.0');
    wp_enqueue_script( 'jquery_3', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js', array(), '3.6.0', true);
    wp_enqueue_script( 'movprod_js', MOVPROD_BASEURI . 'assets/js/movprod-main.js', array('jquery_3'), '1.0.0');
});


/* Convert to radio buttons taxonomy */
function term_movie_genre_radio( $args ) {
    if ( ! empty( $args['taxonomy'] ) && $args['taxonomy'] === 'movie-genre') {
        if ( empty( $args['walker'] ) || is_a( $args['walker'], 'Walker' ) ) { 
            if ( ! class_exists( 'movie_genre_radio' ) ) {
                
                class movie_genre_radio extends Walker_Category_Checklist {
                    function walk( $elements, $max_depth, ...$args ) {
                        $output = parent::walk( $elements, $max_depth, ...$args );
                        $output = str_replace(
                            array( 'type="checkbox"', "type='checkbox'" ),
                            array( 'type="radio"', "type='radio'" ),
                            $output
                        );

                        return $output;
                    }
                }
            }

            $args['walker'] = new movie_genre_radio;
        }
    }

    return $args;
}

add_filter( 'wp_terms_checklist_args', 'term_movie_genre_radio' );