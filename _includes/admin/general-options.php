<?php

if (isset($_POST['update'])) {

    $_POST = array_map('stripslashes_deep', $_POST);

    $apikey = esc_attr($_POST['apikey']);

    $loop_items = esc_attr($_POST['loop_items']);

    update_option('movprod_apikey', $apikey);
    update_option('loop_items', $loop_items);

    echo '<div id="message" class="updated"><p><strong>Settings saved.</strong></p></div>';

} else {
    $apikey = get_option('movprod_apikey');
    $loop_items = get_option('loop_items');
}
?>
<div class="wrap">
    <?php screen_icon(); ?>
    <h2>MovProd - Settings</h2>
    <?php settings_errors(); ?>

    <form action="" method="post" class="submit">
        <table class="widefat">
			<thead>
				<tr>
					<th>API Key (v3 auth)</th>
				</tr>
			</thead>
			<tbody>
                <tr>
                    <td><input type="text" size="40" name="apikey" value="<?php echo $apikey; ?>"></td>
                </tr>
                <tr>
                    <td><input type="number" size="40" name="loop_items" value="<?php echo $loop_items; ?>"></td>
                </tr>
            </tbody>
		</table>

        <?php submit_button(); ?>

        <input type="hidden" name="update" value="1" />
    </form>
</div>