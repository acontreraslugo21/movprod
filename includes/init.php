<?php

/** Block direct access */
defined( 'ABSPATH' ) or die( 'No direct script access allowed' );

/** Constants */
define( 'MOVPROD_TEXTDOMAIN', 'movprod' );

/** Admin menu */
add_action( 'admin_menu', function () {
	$parent_slug = 'movprod';

	/** Menu item */
	add_menu_page( 'MovProd Settings', 'MovProd', 'administrator', $parent_slug, function() {
		include_once MOVPROD_BASEPATH . '/_includes/admin/general-options.php';
	}, 'dashicons-format-video' );

});

// Register Custom Taxonomy
function custom_movie_genre() {

	$labels = array(
		'name'                       => _x( 'Movie Genre', 'Taxonomy General Name', 'movprod' ),
		'singular_name'              => _x( 'Movie Genre', 'Taxonomy Singular Name', 'movprod' ),
		'menu_name'                  => __( 'Movie Genre', 'movprod' ),
		'all_items'                  => __( 'All Items', 'movprod' ),
		'parent_item'                => __( 'Parent Item', 'movprod' ),
		'parent_item_colon'          => __( 'Parent Item:', 'movprod' ),
		'new_item_name'              => __( 'New Item Name', 'movprod' ),
		'add_new_item'               => __( 'Add New Item', 'movprod' ),
		'edit_item'                  => __( 'Edit Item', 'movprod' ),
		'update_item'                => __( 'Update Item', 'movprod' ),
		'view_item'                  => __( 'View Item', 'movprod' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'movprod' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'movprod' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'movprod' ),
		'popular_items'              => __( 'Popular Items', 'movprod' ),
		'search_items'               => __( 'Search Items', 'movprod' ),
		'not_found'                  => __( 'Not Found', 'movprod' ),
		'no_terms'                   => __( 'No items', 'movprod' ),
		'items_list'                 => __( 'Items list', 'movprod' ),
		'items_list_navigation'      => __( 'Items list navigation', 'movprod' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'rewrite'                    => false,
	);
	register_taxonomy( 'movie-genre', array( 'product' ), $args );

}
add_action( 'init', 'custom_movie_genre', 0 );


/*
    Custom Field
*/

add_action('woocommerce_checkout_process', 'customised_checkout_field_process');

function customised_checkout_field_process(){

    if (!$_POST['_movprod_bf']) wc_add_notice(__('Debe seleccionar al menos 1 pelicula antes de realizar el pedido.') , 'error');
}

/**

* Update the value given in custom field

*/

add_action('woocommerce_checkout_update_order_meta', 'custom_checkout_field_update_order_meta');

function custom_checkout_field_update_order_meta($order_id)

{

    if (!empty($_POST['_movprod_bf'])) {
        update_post_meta($order_id, '_movprod_bf',sanitize_text_field($_POST['_movprod_bf']));
    }

}


/**

* Add custom field to the checkout page

*/

add_action('woocommerce_after_order_notes', 'custom_checkout_field');

function custom_checkout_field($checkout){

    woocommerce_form_field('_movprod_bf', array(
        'type' => 'text',
		'required'    => true,
        'class' => array(
        'hidden'
    ) ,
    'label' => __('Películas') ,
    'placeholder' => __('') ,
    ),

    $checkout->get_value('_movprod_bf'));
}

/*
    Adding field to the order resume
*/
add_action('woocommerce_admin_order_data_after_billing_address', 'movprod_billing_fields_display_admin_order_meta', 10, 1);

function movprod_billing_fields_display_admin_order_meta($order) {
    echo '<p><strong>' . __('Lista de peliculas') . ':</strong><br> ' . get_post_meta($order->id, '_movprod_bf', true) . '</p>';
}

/*
    Adding the content after related products
*/

add_action( 'woocommerce_after_single_product_summary', 'custom_single_product_banner', 12 );

add_action( 'woocommerce_after_add_to_cart_form', 'htdat_content_after_addtocart_button' );

function htdat_content_after_addtocart_button() {
    $taxonomy_slug = 'movie-genre';
    if ( is_product()) {
        $terms = get_the_terms( get_the_ID(), $taxonomy_slug );

        if (!empty($terms) && is_array( $terms ) ) {
            echo '<a href="#container-movies" class="btn-movies-woo">Debes seleccionar al menos 1 película</a>';		
        }
    }  
    
}

use Tmdb\Client;

function custom_single_product_banner() {

    $client = require_once MOVPROD_BASEPATH . '/vendor/php-tmdb/api/examples/setup-client.php';

    global $product;

    $taxonomy_slug = 'movie-genre';
    $terms = get_the_terms( get_the_ID(), $taxonomy_slug );


    if (!empty($terms) && is_array( $terms ) ) {
        foreach ($terms as $term ) {
            $genre_term = $term->name;
        }
   
    $all_genres = $client->getGenresApi()->getGenres();
    $all_genres_r = $all_genres["genres"];
    
    foreach ($all_genres_r as $genre_post) {
        $genre_id = $genre_post['id'];
        $genre_name = $genre_post['name'];

        if($genre_term == $genre_name){
            $id_g = $genre_id;
        }
    }

    $genres = $client->getGenresApi()->getMovies($id_g);
    $genres_result = $genres["results"];
    ?>
    <div id="container-movies" class="container-movies">
        <div class="row">
            <div class="col-12">
                <h2>Choose your favorite movies</h2>
            </div>
            <?php
                $count = 0;
                foreach ($genres_result as $genre) {
                    //var_dump($genre);
                    $title = $genre['original_title']; 
                    $image = $genre['poster_path']
                    ?>
                            <div class="col-md p-0">
                                <label class="cb-img-block">
                                    <input class="cb-img-block--input" type="checkbox" name="movie[]" value="<?php echo $title; ?>" />
                                    <div class="cb-img-block--img" style="background-image:url(https://image.tmdb.org/t/p/w500/<?php echo $image; ?>)"></div>
                                    <div class="cb-title">
                                        <?php echo $title; ?>
                                    </div>
                                </label>
                            </div>
                    <?php
                    if (++$count == get_option('loop_items')) break;
                }
            ?>
        </div>
    </div>
    <?php
    }
}